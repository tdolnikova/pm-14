package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public final class DataSaveFasterxmlXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_SAVE_FASTERXML_XML;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_SAVE_FASTERXML_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<UserDTO> users = serviceLocator.getUserEndpoint().findAllUser(serviceLocator.getSessionDTO());
        List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO());
        List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSessionDTO());
        boolean userSaveAllowed = serviceLocator.getUserEndpoint().saveFasterxmlXmlUser(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), users);
        if (userSaveAllowed) System.out.println("ПОЛЬЗОВАТЕЛИ СОХРАНЕНЫ");
        boolean taskSaveAllowed = serviceLocator.getTaskEndpoint().saveFasterxmlXmlTask(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), tasks);
        if (taskSaveAllowed) System.out.println("ЗАДАЧИ СОХРАНЕНЫ");
        boolean projectSaveAllowed = serviceLocator.getProjectEndpoint().saveFasterxmlXmlProject(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), projects);
        if (projectSaveAllowed) System.out.println("ПРОЕКТЫ СОХРАНЕНЫ");
    }

    @Override
    public boolean isSecure() {
        return (!(serviceLocator.getUserDTO() == null));
    }

}
