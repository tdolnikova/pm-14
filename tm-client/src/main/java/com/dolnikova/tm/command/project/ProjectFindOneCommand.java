package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return;
        }
        System.out.println(AdditionalMessage.INSERT_PROJECT_NAME);
        @Nullable ProjectDTO projectDTO = null;
        while (projectDTO == null) {
            @Nullable final String projectName = Bootstrap.scanner.nextLine();
            if (projectName == null || projectName.isEmpty()) break;
            projectDTO = serviceLocator.getProjectEndpoint().findOneByNameProject(serviceLocator.getSessionDTO(), projectName);
            if (projectDTO == null) System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
            else System.out.println(AdditionalMessage.PROJECT_NAME + projectDTO.getName());
        }
    }

    @Override
    public boolean isSecure() {
        return (serviceLocator.getUserDTO() != null);
    }
}
