package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;
import org.jetbrains.annotations.NotNull;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}