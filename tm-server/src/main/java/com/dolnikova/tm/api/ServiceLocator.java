package com.dolnikova.tm.api;

import com.dolnikova.tm.api.service.*;
import com.dolnikova.tm.mapper.ProjectMapper;
import com.dolnikova.tm.mapper.SessionMapper;
import com.dolnikova.tm.mapper.TaskMapper;
import com.dolnikova.tm.mapper.UserMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull IUserService getUserService();
    @NotNull ISessionService getSessionService();
}
