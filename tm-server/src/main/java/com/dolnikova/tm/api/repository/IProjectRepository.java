package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.mapper.ProjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    Project findOneById(@NotNull final String userId, @NotNull final String id);

    Project findOneByName(@NotNull final String name);

    @Nullable List<Project> findAllByName(@NotNull final String ownerId, @NotNull final String text);

    @Nullable List<Project> findAllByDescription(@NotNull final String ownerId, @NotNull final String text);

    void merge(@NotNull final String newData,
               @NotNull final Project entityToMerge,
               @NotNull final DataType dataType);

    @Override
    @Nullable List<Project> findAll(final @NotNull String userId);

    @Override
    void persist(final @NotNull Project entity);

    @Override
    void remove(final @NotNull Project entity);

    @Override
    void saveBin(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<Project> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<Project> entities) throws Exception;

    @Nullable
    @Override
    List<Project> loadBin() throws Exception;

    @Nullable
    @Override
    List<Project> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<Project> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<Project> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<Project> loadJaxbXml() throws Exception;
}
