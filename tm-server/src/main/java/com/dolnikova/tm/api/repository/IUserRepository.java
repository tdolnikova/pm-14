package com.dolnikova.tm.api.repository;

import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull final String login);

    @Nullable
    List<User> findAllByLogin(@NotNull final String ownerId, @NotNull final String login);

    void merge(@NotNull final String newData,
               @NotNull final User entityToMerge,
               @NotNull final DataType dataType);

    @Override
    void saveBin(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbJson(final @NotNull List<User> entities) throws Exception;

    @Override
    void saveJaxbXml(final @NotNull List<User> entities) throws Exception;

    @Nullable
    @Override
    List<User> loadBin() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlJson() throws Exception;

    @Nullable
    @Override
    List<User> loadFasterxmlXml() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbJson() throws Exception;

    @Nullable
    @Override
    List<User> loadJaxbXml() throws Exception;
}
