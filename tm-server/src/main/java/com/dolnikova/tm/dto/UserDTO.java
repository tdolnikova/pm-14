package com.dolnikova.tm.dto;

import com.dolnikova.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO extends AbstractDTO {

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String login;

    @Nullable
    private String middleName;

    @Nullable
    private String passwordHash;

    @Nullable
    private String phone;

    @Nullable
    private boolean locked;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role;

}
