package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.DtoConversionUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@WebService
public class UserEndpoint extends AbstractEndpoint {

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    @Nullable
    public UserDTO findOneByLoginUser(@WebParam(name = "name") @Nullable String login) {
        return DtoConversionUtil.userToDto(userService.findOneByLogin(login));
    }

    @WebMethod
    public @Nullable UserDTO findOneBySession(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return DtoConversionUtil.userToDto(userService.findOneBySession(session));
    }

    @WebMethod
    public @Nullable List<UserDTO> findAllUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (session == null) return null;
        @Nullable final List<User> users = userService.findAll(session.getUser().getId());
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> findAllByLoginUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                                                      @WebParam(name = "text") @Nullable String text) {

        if (sessionDTO == null) return null;
        @Nullable final User currentUser = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (session == null) return null;
        @Nullable final String userId = session.getUser().getId();
        @Nullable final List<User> users = userService.findAllByLogin(userId, text);
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public void persistUser(@WebParam(name = "userDTO") @Nullable UserDTO userDTO) {
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        if (user == null) return;
        user.setId(UUID.randomUUID().toString());
        userService.persist(user);
    }

    @WebMethod
    public void persistListUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                                @WebParam(name = "users") @Nullable List<UserDTO> list) {
        if (sessionDTO == null || list == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userDTO : list) {
            userList.add(DtoConversionUtil.dtoToUser(userDTO));
        }
        userService.persistList(userList);
    }

    @WebMethod
    public void mergeUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                          @WebParam(name = "newData") @Nullable String newData,
                          @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
                          @WebParam(name = "dataType") @Nullable DataType dataType) {
        if (sessionDTO == null) return;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        userService.merge(newData, user, dataType);
    }

    @WebMethod
    public void removeUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
                           @WebParam(name = "userDTO") @Nullable UserDTO userDTO) {

        if (sessionDTO == null) return;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        userService.remove(user);
    }

    @WebMethod
    public void removeAllUser(@WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        if (session == null) return;
        userService.removeAll(session.getUser().getId());
    }

    @WebMethod
    public boolean checkPassword(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userInput") @Nullable String userInput) {
        if (sessionDTO == null) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return false;
        return userService.checkPassword(session.getUser().getId(), userInput);
    }

    @WebMethod
    public boolean saveBinUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
            @WebParam(name = "users") @Nullable List<UserDTO> userDTOS) throws Exception {
        if (sessionDTO == null) return false;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (user == null) return false;
        if (user.getRole() != Role.ADMIN) return false;
        if (userDTOS == null || userDTOS.isEmpty()) return true;
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userdto : userDTOS) {
            userList.add(DtoConversionUtil.dtoToUser(userdto));
        }
        userService.saveBin(userList);
        return true;
    }



    @WebMethod
    public boolean saveFasterxmlJsonUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
            @WebParam(name = "users") @Nullable List<UserDTO> userDTOS) throws Exception {
        if (sessionDTO == null) return false;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (user == null) return false;
        if (user.getRole() != Role.ADMIN) return false;
        if (userDTOS == null || userDTOS.isEmpty()) return true;
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userdto : userDTOS) {
            userList.add(DtoConversionUtil.dtoToUser(userdto));
        }
        userService.saveFasterxmlJson(userList);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
            @WebParam(name = "users") @Nullable List<UserDTO> userDTOS) throws Exception {
        if (sessionDTO == null) return false;
        @Nullable final User user = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (user == null) return false;
        if (user.getRole() != Role.ADMIN) return false;
        if (userDTOS == null || userDTOS.isEmpty()) return true;
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userdto : userDTOS) {
            userList.add(DtoConversionUtil.dtoToUser(userdto));
        }
        userService.saveFasterxmlXml(userList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
            @WebParam(name = "users") @Nullable List<UserDTO> userDTOS) throws Exception {
        if (sessionDTO == null) return false;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return false;
        if (currentUser.getRole() != Role.ADMIN) return false;
        if (userDTOS == null || userDTOS.isEmpty()) return true;
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userdto : userDTOS) {
            userList.add(DtoConversionUtil.dtoToUser(userdto));
        }
        userService.saveJaxbJson(userList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO,
            @WebParam(name = "users") @Nullable List<UserDTO> userDTOS) throws Exception {
        if (sessionDTO == null) return false;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return false;
        if (currentUser.getRole() != Role.ADMIN) return false;
        if (userDTOS == null || userDTOS.isEmpty()) return true;
        @Nullable final List<User> userList = new ArrayList<>();
        for (UserDTO userdto : userDTOS) {
            userList.add(DtoConversionUtil.dtoToUser(userdto));
        }
        userService.saveJaxbXml(userList);
        return true;
    }

    @WebMethod
    public @Nullable List<UserDTO> loadBinUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return null;
        if (currentUser.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<User> users = userService.loadBin();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> loadFasterxmlJsonUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return null;
        if (currentUser.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<User> users = userService.loadFasterxmlJson();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> loadFasterxmlXmlUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return null;
        if (currentUser.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<User> users = userService.loadFasterxmlXml();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> loadJaxbJsonUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return null;
        if (currentUser.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<User> users = userService.loadJaxbJson();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

    @WebMethod
    public @Nullable List<UserDTO> loadJaxbXmlUser(
            @WebParam(name = "sessionDTO") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "userDTO") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User currentUser = DtoConversionUtil.dtoToUser(userDTO);
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, currentUser);
        validate(session);
        if (currentUser == null) return null;
        if (currentUser.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<User> users = userService.loadJaxbXml();
        if (users == null) return null;
        @Nullable final List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(DtoConversionUtil.userToDto(user));
        }
        return userDTOs;
    }

}
