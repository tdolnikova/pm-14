package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public final class User extends AbstractEntity implements Serializable {

    private String email;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String login;

    private String middleName;

    private String passwordHash;

    private String phone;

    private boolean locked;

    @Enumerated(value = EnumType.STRING)
    private Role role;

}
