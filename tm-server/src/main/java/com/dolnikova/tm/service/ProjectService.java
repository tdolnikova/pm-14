package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IProjectService;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.util.HibernateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final Logger LOGGER = Logger.getLogger(ProjectService.class.getName());

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск проекта по id]");
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Project findOneByName(@Nullable final String name) {
        LOGGER.info("[Поиск проекта по названию]");
        if (name == null || name.isEmpty()) return null;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findOneByName(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable final String userId) {
        LOGGER.info("[Поиск всех проектов]");
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Project> findAllByName(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех проектов по названию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findAllByName(userId, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Project> findAllByDescription(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех проектов по описанию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            return projectRepository.findAllByDescription(userId, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void persist(@Nullable final Project project) {
        LOGGER.info("[Вставка проекта]");
        if (project == null) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.persist(project);
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
    }

    @Override
    public void persistList(@Nullable List<Project> list) {
        if (list == null || list.isEmpty()) return;
        for (Project project : list) persist(project);
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Project entityToMerge,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных проекта]");
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        @Nullable final Project dbProject = findOneById(entityToMerge.getUser().getId(), entityToMerge.getId());
        if (dbProject == null) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        try {
            projectRepository.merge(newData, dbProject, dataType);
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        }
        entityManager.getTransaction().commit();
    }

    @Override
    public void remove(@Nullable final Project project) {
        LOGGER.info("[Удаление проекта]");
        if (project == null) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        //@Nullable final Project dbProject = entityManager.find(Project.class, project.getId());
       // if (dbProject == null) return;
        entityManager.getTransaction().begin();
        try {
            projectRepository.remove(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        LOGGER.info("[Удаление всех проектов пользователя]");
        if (userId == null || userId.isEmpty()) return;
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null || projects.isEmpty()) return;
        for (Project project : projects) {
            remove(project);
        }
    }

    @Override
    public void saveBin(@Nullable List<Project> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Project> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Project> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.saveFasterxmlXml(entities);
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@Nullable List<Project> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Project> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Project> loadBin() throws Exception {
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.loadBin();
    }

    @Override
    public @Nullable List<Project> loadFasterxmlJson() throws Exception {
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Project> loadFasterxmlXml() throws Exception {
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Project> loadJaxbJson() throws Exception {
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Project> loadJaxbXml() throws Exception {
        @NotNull final EntityManager entityManager = HibernateUtil.factory().createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.loadJaxbXml();
    }

}
